// 02Ce Recipe Program by Salma Omar (somar1)
#include <iostream>
using namespace std;

int main()
{
    // DECLARING CONST
    const float DARK_CHOCO = 8;
    const float SALT = 1;
    const float CAYENNE_PEPPER = 1;
    const float EGG = 6;
    const float WHITE_SUGAR = 1.0/3;
    const float WHOLE_MILK = 1;
    const float HEAVY_CREAM = 5.0/4;
    const float VANILLA_EXTRACT = 1.0/4;
    const float UNSALTED_BUTTER = 1;
    const float HEAVY_CREAM2 = 1.0/4;
    const float VANILLA_EXTRACT2 = 1.0/8;
    const float SHAVED_DARKCHOCO = 2;
   
    
    //PROMPT USER FOR NUMBER OF BATCHES
    float batches;
    cout << "How many batches? ";
    cin >> batches;
   
    
    cout << "Chocolate Puddino" << endl;
    cout << "https://www.allrecipes.com/recipe/283307/chocolate-puddino/" << endl;
    cout << endl;
    cout << "INGREDIENTS:" << endl;
    cout <<"--------------------------------------------" << endl;
    cout << DARK_CHOCO * batches <<" ounces dark chocolate chips" << endl;
    cout << SALT * batches<< " pinch salt" << endl;
    cout << CAYENNE_PEPPER * batches << " pinch cayenne pepper" << endl;
    cout << EGG * batches << " large eggs" << endl;
    cout << WHITE_SUGAR * batches << " cup white sugar" << endl;
    cout << WHOLE_MILK * batches << " cup whole milk" << endl;
    cout << HEAVY_CREAM * batches << " cups heavy cream" << endl;
    cout << VANILLA_EXTRACT * batches  << " teaspoon vanilla extract" << endl;
    cout << UNSALTED_BUTTER * batches << " tablespoon unsalted butter" << endl;
    cout <<"--------------------------------------------" << endl;
    cout << endl;
    cout << "TOPPINGS:" << endl;
    cout <<"--------------------------------------------" << endl;
    cout << HEAVY_CREAM2 * batches << " cup heavy cream" << endl;
    cout << VANILLA_EXTRACT2 * batches << " teaspoon vanilla extract" << endl;
    cout << SHAVED_DARKCHOCO * batches << " tablespoons shaved dark chocolate" << endl;
    cout <<"--------------------------------------------" << endl;
    cout << endl;
    cout << "DIRECTIONS" << endl;
    cout << "1. Combine chocolate chips, salt, and cayenne in a heat-proof measuring cup; set aside." << endl;
    cout << endl;
    cout << "2. Separate eggs by cracking one egg into your hand over a bowl. Open your fingers slightly and gently jiggle your hand until the egg white falls into the bowl below. Transfer the yolk to a skillet. Repeat with remaining eggs. Reserve egg whites for another use." << endl;
    cout << endl;
        cout << "3. Add sugar, milk, and cream to egg yolks. Whisk thoroughly, breaking egg yolks first, until well combined." << endl;
    cout << endl;
    cout << "4. Place skillet on the stove over medium or medium-low heat. Cook, stirring constantly with a silicone spoon, until very hot and thick enough to coat the back of the spoon, about 5 minutes. An instant-read thermometer should read at least 175 degrees F (79 degrees C). Remove from the heat." << endl;
    cout << endl;
    cout << "5. Set a fine sieve over the bowl of reserved chocolate. Strain the custard sauce into the chocolate and let sit for 2 minutes. Whisk until chocolate has melted and custard sauce is smooth and shiny, about 2 minutes. Add vanilla and butter; stir until butter has melted, about 1 minute." << endl;
    cout << endl;
    cout << "6. Pour warm custard sauce into 6 serving glasses. Tilt each glass and rotate it around so the warm chocolate coats another 1/2 inch of the glass. Cover with plastic and place in the refrigerator until completely chilled, at least 3 to 4 hours." << endl;
    cout << endl;
    cout << "7. Combine cream and vanilla extract for topping in a metal bowl and whisk until thickened; make sure no peaks form. Spoon cream into the glasses, then tilt and twirl to coat the sides a bit." << endl;
    cout << endl;
    cout << "8. Garnish with shaved chocolate and serve:)!" << endl;
        
        
    

    return 0;
}
