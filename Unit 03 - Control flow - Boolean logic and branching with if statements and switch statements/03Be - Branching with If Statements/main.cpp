 main.cpp
//  03Be - Salma Omar (somar11)
//
//  Created by Apple Owner on 2/24/21.
//

#include <iostream>
using namespace std;


int main()
    // Program 1 if statement
{
    float withdrawAmount;
    float bankBalance;
    
    cout << "PROGRAM 1" << endl;
    cout << "What is your bank balance?" << endl;
    cin >> bankBalance;
    cout << "How much did you withdraw?" << endl;
    cin >> withdrawAmount;
    bankBalance = bankBalance - withdrawAmount;
    cout << "Balance: " <<  bankBalance;
    
    if (bankBalance < 0)
    {
        cout << " (OVERDRAWN!)" << endl;
    }
   
    // Program 2 if/else statement
    cout << "PROGRAM 2" << endl;
    float earnedPoints;
    float totalPoints;
    
    cout << "How many points did you score? "; cin >> earnedPoints;
    cout << "How many total points were there? "; cin >> totalPoints;
    
    float result = earnedPoints / totalPoints;
    
    if ( result >= .50)
    {
        cout << "Pass" << endl;
    }
    else
    {
        cout << "Fail" << endl;
    }
    
    
    // Program 3 if/else if/else statement
     
    cout << "PROGRAM 3" << endl;
    int charge;
    
    cout << "Enter your phone percentage: "; cin >> charge;
    
    if (charge >= 95)
    {
        cout << "[****]" << endl;
    }
    else if (charge >= 75)
    {
        cout << "[***_]" << endl;
    }
    else if (charge >= 50)
    {
        cout << "[**__]" << endl;
    }
    else if (charge >= 25)
    {
        cout << "[*___]" << endl;
    }
    else
    {
        cout << "[____]" << endl;
    }
    
    // Program && operators
    cout << "PROGRAM 4" << endl;
    int choice;
    
    cout << "1. Water" << endl;
    cout << "2. Coffee" << endl;
    cout << "3. Milk" << endl;
    cout << "4. Chocolate Milk" << endl;
    cout << "5. Orange Juice" << endl;
    
    cout << "Select a drink: " << endl;
    cin >> choice;
    
    if (choice >= 1 && choice <= 5)
    {
        cout << "Good Choice!" << endl;
    }
    else
    {
        cout << "Invalid Choice" << endl;
    }
     
     // Program || operators
    cout << "PROGRAM 5" << endl;
    int choice2;
    
    cout << "Enter a number between 1 and 10: ";
    cin >> choice2;
    
    if ( choice2 < 1 || choice2 > 10 )
    {
        cout << "Invalid Choice!" << endl;
    }
    else
    {
        cout << "Valid Choice!" << endl;
    }
    
    return 0;
}
